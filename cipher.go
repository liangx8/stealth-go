package main

import (
	"crypto/md5"
	"fmt"
	"crypto/aes"
	"crypto/rand"
	"crypto/cipher"
)

func GenKey(rawKey []byte,size int) []byte{
	h := md5.New()
	const md5len = 16
	md5sum := func(in []byte) []byte{
		h.Reset()
		h.Write(in)
		return h.Sum(nil)
	}
	blkcnt := (size-1) / md5len + 1
	key := make([]byte,blkcnt * md5len)
	nextkey := make([]byte,md5len+len(rawKey))
	copy(key[:md5len],md5sum(rawKey))
	
	copy(nextkey[md5len:],rawKey)
	fmt.Println(nextkey)
	for i:=1;i<blkcnt;i++ {
		pos := (i-1) * md5len
		copy(nextkey,key[pos:pos+md5len])
		fmt.Println(nextkey)
		copy(key[pos+md5len:],md5sum(nextkey))
	}
	return key[:size]
}



func main(){
	key := GenKey([]byte(aString),24)
	iv := key[:aes.BlockSize]
	blk,err:=aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	if _,err := rand.Read(iv); err != nil {
		panic(err)
	}
	ciphertext := make([]byte,len(plaintext))
	enc := cipher.NewCFBEncrypter(blk,iv)
	enc.XORKeyStream(ciphertext,[]byte(plaintext))
	fmt.Print(" plain text:")
	fmt.Println([]byte(plaintext))
	fmt.Print("cipher text:")
	fmt.Println(ciphertext)
	for i,_ := range ciphertext {
		ciphertext[i]=0
	}
	enc = cipher.NewCFBEncrypter(blk,iv)
	enc.XORKeyStream(ciphertext,[]byte(plaintext)[:4])
	fmt.Println(ciphertext)
	enc.XORKeyStream(ciphertext[4:],[]byte(plaintext)[4:6])
	fmt.Println(ciphertext)
}

	const (
		aString = "Hello"
		plaintext = "0123456789abcdef"
	)
